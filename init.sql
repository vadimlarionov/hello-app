CREATE TABLE users (
    id SERIAL NOT NULL PRIMARY KEY,
    name TEXT NOT NULL,
    is_male BOOLEAN NOT NULL,
    count_views INT NOT NULL DEFAULT 0,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

CREATE TABLE sessions (
    access_token TEXT NOT NULL PRIMARY KEY ,
    user_id INTEGER REFERENCES users (id),
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    last_visited_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);
