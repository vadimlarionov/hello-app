package main

import (
	"encoding/base64"
	"io"
	"log"
	"math/rand"
	"net"
	"strings"
	"time"

	"gitlab.com/vadimlarionov/hello-app/internal/postgres"
	"gitlab.com/vadimlarionov/hello-app/internal/services"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"gopkg.in/alecthomas/kingpin.v2"
)

type Config struct {
	ListenAddr  string
	DB          postgres.Config
	Base64DBURL string
}

func parseFlags() Config {
	var cfg Config

	kingpin.Flag("listen-addr", "Listen address.").
		Envar("LISTEN_ADDR").Default(":9000").
		StringVar(&cfg.ListenAddr)

	kingpin.Flag("db-url", "DB URL.").
		Envar("DB_URL").Default("postgres://user:passwd@localhost:5432/hello_db?sslmode=disable&fallback_application_name=hello-api").
		StringVar(&cfg.DB.URL)
	kingpin.Flag("db-max-conn", "DB max connections.").
		Envar("DB_MAX_CONN").Default("2").
		IntVar(&cfg.DB.MaxConnections)
	kingpin.Flag("db-max-conn-lifetime", "DB max connection lifetime.").
		Envar("DB_MAX_CONN_LIFETIME").Default("5m").
		DurationVar(&cfg.DB.MaxConnLifetime)
	kingpin.Flag("base64-db-url", "DB URL encoded by base64.").
		Envar("BASE64_DB_URL").Default("").
		StringVar(&cfg.Base64DBURL)

	kingpin.Parse()

	// It's a hack to get env variable value from gitlab
	if cfg.Base64DBURL != "" {
		dbURL, err := base64.StdEncoding.DecodeString(cfg.Base64DBURL)
		if err != nil {
			log.Fatalf("Can't parse base64 db url: %s", err)
		}
		cfg.DB.URL = strings.TrimSpace(string(dbURL))
	}

	return cfg
}

func main() {
	cfg := parseFlags()
	rand.Seed(time.Now().UnixNano()) // To generate random names

	logger, err := zap.NewDevelopment()
	if err != nil {
		log.Fatal("Can't create zap logger: ", err)
	}
	defer logger.Sync() // nolint:errcheck

	listener, err := net.Listen("tcp", cfg.ListenAddr)
	if err != nil {
		logger.Sugar().Fatalf("Can't listen on port %s: %v", cfg.ListenAddr, err)
	}
	defer handleCloser(logger, "listener", listener)

	db, err := postgres.New(logger, cfg.DB)
	if err != nil {
		logger.Sugar().Fatalf("Can't create db: %s", err)
	}
	defer handleCloser(logger, "db", db)

	if err = db.CheckConnection(); err != nil {
		logger.Sugar().Fatalf("Can't check connection: %s", err)
	}

	userStorage, err := postgres.NewUserStorage(db)
	if err != nil {
		logger.Sugar().Fatalf("Can't create user storage: %s", err)
	}
	defer handleCloser(logger, "user_storage", userStorage)

	server, err := NewServer(logger, userStorage)
	if err != nil {
		logger.Sugar().Fatalf("Can't create server: %s", err)
	}

	s := grpc.NewServer()
	services.RegisterHelloServiceServer(s, server)

	logger.Sugar().Infof("Server started on %s", cfg.ListenAddr)
	if err = s.Serve(listener); err != nil {
		logger.Sugar().Fatalf("Can't serve requests: %s", err)
	}
}

func handleCloser(logger *zap.Logger, resource string, closer io.Closer) {
	if err := closer.Close(); err != nil {
		logger.Sugar().Errorf("Can't close %q: %s", resource, err)
	}
}
