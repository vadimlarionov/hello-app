package main

import (
	"bytes"
	"context"
	"html/template"
	"math/rand"

	"github.com/pkg/errors"
	"gitlab.com/vadimlarionov/hello-app/internal/animals"
	"gitlab.com/vadimlarionov/hello-app/internal/services"
	"gitlab.com/vadimlarionov/hello-app/internal/users"
	"go.uber.org/zap"
)

var _ services.HelloServiceServer = &Server{}

type Server struct {
	logger      *zap.SugaredLogger
	userStorage users.Storage
	tmpl        *template.Template
}

func NewServer(logger *zap.Logger, userStorage users.Storage) (*Server, error) {
	tmpl, err := template.New("index").Parse(htmlTemplate)
	if err != nil {
		return nil, errors.Wrap(err, "can't parse html template")
	}

	return &Server{
		logger:      logger.Sugar(),
		userStorage: userStorage,
		tmpl:        tmpl,
	}, nil
}

func (s *Server) CreateUser(ctx context.Context, _ *services.Empty) (*services.CreateUserResponse, error) {
	isMale := rand.Int()%2 == 0 // nolint:gosec
	u := users.User{
		Name:   animals.Rand(isMale),
		IsMale: isMale,
	}

	if err := s.userStorage.Create(&u); err != nil {
		return nil, err
	}

	resp := services.CreateUserResponse{
		UserId:   u.ID,
		Username: u.Name,
	}

	s.logger.Infof("User %d %s is registered", u.ID, u.Name)
	return &resp, nil
}

func (s *Server) Hello(ctx context.Context, r *services.HelloRequest) (*services.HTMLResponse, error) {
	if err := s.userStorage.IncCountViews(r.UserId); err != nil {
		return nil, errors.Wrapf(err, "can't increment user %d views counter", r.UserId)
	}

	u, err := s.userStorage.Find(r.UserId)
	if err != nil {
		return nil, errors.Wrapf(err, "can't find user by id %d", r.UserId)
	}

	data := map[string]interface{}{
		"Name":       u.Name,
		"CountViews": u.CountViews,
		"IsFemale":   !u.IsMale,
	}

	var b bytes.Buffer
	if err = s.tmpl.Execute(&b, data); err != nil {
		return nil, errors.Wrapf(err, "can't execute template (user_id=%d)", u.ID)
	}

	return &services.HTMLResponse{
		Content: b.Bytes(),
	}, nil
}
