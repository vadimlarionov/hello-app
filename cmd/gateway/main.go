package main

import (
	"encoding/base64"
	"io"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"gitlab.com/vadimlarionov/hello-app/internal/postgres"
	"gitlab.com/vadimlarionov/hello-app/internal/services"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"gopkg.in/alecthomas/kingpin.v2"
)

//go:generate protoc ../../internal/services/hello_service.proto --proto_path=../../internal/services --go_out=plugins=grpc:../../internal/services

type Config struct {
	ListenAddr   string
	HelloAPIAddr string
	DB           postgres.Config
	Base64DBURL  string
}

func parseFlags() Config {
	var cfg Config

	kingpin.Flag("listen-addr", "Listen address.").
		Envar("LISTEN_ADDR").Default(":8080").
		StringVar(&cfg.ListenAddr)
	kingpin.Flag("hello-api-addr", "Hello API address.").
		Envar("HELLO_API_ADDR").Default("localhost:9000").
		StringVar(&cfg.HelloAPIAddr)

	kingpin.Flag("db-url", "DB URL.").
		Envar("DB_URL").Default("postgres://user:passwd@localhost:5432/hello_db?sslmode=disable&fallback_application_name=hello-api").
		StringVar(&cfg.DB.URL)
	kingpin.Flag("db-max-conn", "DB max connections.").
		Envar("DB_MAX_CONN").Default("2").
		IntVar(&cfg.DB.MaxConnections)
	kingpin.Flag("db-max-conn-lifetime", "DB max connection lifetime.").
		Envar("DB_MAX_CONN_LIFETIME").Default("5m").
		DurationVar(&cfg.DB.MaxConnLifetime)
	kingpin.Flag("base64-db-url", "DB URL encoded by base64.").
		Envar("BASE64_DB_URL").Default("").
		StringVar(&cfg.Base64DBURL)

	kingpin.Parse()

	// It's a hack to get env variable value from gitlab
	if cfg.Base64DBURL != "" {
		dbURL, err := base64.StdEncoding.DecodeString(cfg.Base64DBURL)
		if err != nil {
			log.Fatalf("Can't parse base64 db url: %s", err)
		}
		cfg.DB.URL = strings.TrimSpace(string(dbURL))
	}

	return cfg
}

// nolint:gocyclo
func main() {
	cfg := parseFlags()

	logger, err := zap.NewDevelopment()
	if err != nil {
		log.Fatal("Can't create zap logger: ", err)
	}
	defer logger.Sync() // nolint:errcheck

	cc, err := grpc.Dial(cfg.HelloAPIAddr, grpc.WithInsecure())
	if err != nil {
		logger.Sugar().Fatal("Can't create connection to hello-api: ", err)
	}
	defer handleCloser(logger, "hello-api-connection", cc)

	helloClient := services.NewHelloServiceClient(cc)

	db, err := postgres.New(logger, cfg.DB)
	if err != nil {
		logger.Sugar().Fatal("Can't create db: ", err)
	}
	defer handleCloser(logger, "db", db)

	if err = db.CheckConnection(); err != nil {
		logger.Sugar().Fatalf("Can't check connection: %s", err)
	}

	userStorage, err := postgres.NewUserStorage(db)
	if err != nil {
		logger.Sugar().Fatal("Can't create user storage: ", err)
	}
	defer handleCloser(logger, "user_storage", userStorage)

	sessionStorage, err := postgres.NewSessionStorage(db)
	if err != nil {
		logger.Sugar().Fatal("Can't create session storage: ", err)
	}
	defer handleCloser(logger, "session_storage", sessionStorage)

	h := NewHandler(logger, helloClient, sessionStorage)
	r := routes(h)

	logger.Sugar().Infof("Server started on %s", cfg.ListenAddr)
	if err := http.ListenAndServe(cfg.ListenAddr, r); err != nil {
		logger.Sugar().Fatalf("Can't listen and serve: %s", err)
	}
}

func routes(h *Handler) *chi.Mux {
	r := chi.NewRouter()

	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middleware.Timeout(60 * time.Second))

	r.Mount("/", h.Routes())

	return r
}

func handleCloser(logger *zap.Logger, resource string, closer io.Closer) {
	if err := closer.Close(); err != nil {
		logger.Sugar().Errorf("Can't close %q: %s", resource, err)
	}
}
